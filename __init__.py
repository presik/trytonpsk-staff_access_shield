# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .access import (Access, AccessStation)
from .employee import Employee

def register():
    Pool.register(
        Employee,
        AccessStation,
        Access,
        module='staff_access_shield', type_='model')
