# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import datetime
from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView, ModelSQL, fields
from trytond.rpc import RPC

__all__ = ['Access', 'AccessStation']


class Access:
    __metaclass__ = PoolMeta
    __name__ = 'staff.access'
    station = fields.Many2One('staff.access.station', 'Access Station')

    @classmethod
    def __setup__(cls):
        super(Access, cls).__setup__()
        cls.__rpc__.update({
            'create_access': RPC(readonly=False, instantiate=0),
            '_create_from_code': RPC(readonly=False, instantiate=0),
            'close_access': RPC(readonly=False, instantiate=0),
        })

    @classmethod
    def create_access(cls, employee_id):
        """Create an access record."""
        cls.create([{
            'employee': employee_id,
            'state': 'open',
        }])

    @classmethod
    def _create_from_code(cls, employee_ids, code):
        """Search the employee and create an access record."""
        Employee = Pool().get('company.employee')
        employees = Employee.search([
            ('code', '=', code),
        ])
        res = {}
        if employees and len(employees) == 1:
            employee = employees[0]
            cls.create_access(employee.id)
            print(employee.party.name)
            res['id'] = employee.id
            res['name'] = employee.party.name
            return employee.party.name
        else:
            return None

    @classmethod
    def close_access(cls, access_id):
        """Close previous access."""
        access = cls(access_id)
        cls.write([access], {
            'state': 'close',
            'exit_timestamp': datetime.datetime.now(),
        })


class AccessStation(ModelSQL, ModelView):
    """Access Station."""
    __name__ = 'staff.access.station'
    name = fields.Char("Name", required=True)
    description = fields.Char("Description")

    @classmethod
    def __setup__(cls):
        super(AccessStation, cls).__setup__()
