# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.rpc import RPC

__all__ = ['Employee']


class Employee:
    __metaclass__ = PoolMeta
    __name__ = 'company.employee'

    @classmethod
    def __setup__(cls):
        super(Employee, cls).__setup__()
        cls.__rpc__.update({
            '_get_employees': RPC(readonly=True, instantiate=0),
        })

    @classmethod
    def _get_employees(cls, employees_ids=[]):
        res = {}
        employees = cls.search([])
        for employee in employees:
            if employee.code:
                res[employee.code] = {
                    'id': employee.id,
                    'name': employee.party.name,
                }
        return res
